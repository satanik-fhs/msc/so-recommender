import abc
from typing import List

from django.db.models import Model


class Recommender(abc.ABC):
    @abc.abstractmethod
    def recommend(self, a: Model, top_n: int = 10) -> List[int]:
        pass
