import os
from enum import Enum
from typing import List, Dict, Tuple, Union

from django.core.management import BaseCommand
from django.db.models import QuerySet
from spacy.tokens import Doc

from app.models import User, Post
from app.utils import cache
from app.utils import Logger
from app.blackbox.models import LanguageModel, Doc2VecModel, ItemModel

logger = Logger.get()

QKey: str = "question"
AKey: str = "answer"


class Method(Enum):
    DOC2VEC = 'doc2vec'


class Command(BaseCommand):

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.user_id: int = -1
        self.model: Union[str, None] = None
        self.method: Union[str, None] = None
        self.data: Dict[str, List[Post]] = {}

    def add_arguments(self, parser):
        parser.add_argument('user_id', type=int)
        parser.add_argument('--method', type=str, choices=[m.value for m in Method], default=Method.DOC2VEC.value)
        parser.add_argument('--model', type=str, description="path to the gensim model")

    def handle(self, *args, **options):
        logger.info("creating recommendations: start")
        (
            self.user_id,
            self.method,
            self.model,
        ) = [options[k] for k in ('user_id', 'method', 'model',)]

        self._recommend()
        logger.info("creating recommendations: done")

    def _collect(self, user: User):

        for key in [QKey, AKey]:
            logger.info("fetching %ss of user [%d]: start" % (key, self.user_id))
            posts: QuerySet = Post.objects\
                .filter(user__id=user.id)\
                .filter(post_type_id=Post.Type[key.upper()].value)
            items: List[Post] = cache.get_from_query("%ss" % key, posts)
            self.data[key] = items

            logger.info("fetching %ss of user [%d]: done" % (key, self.user_id))

    def _recommend(self):
        LanguageModel.create()

        if not os.path.isfile(self.model):
            logger.error("[%s] is not a file" % self.model)

        logger.info("fetching user [%d]" % self.user_id)
        user: User = User.objects.filter(id=self.user_id).first()

        if not user:
            logger.error("user with [%d] does not exist" % self.user_id)
            exit(0)

        self._collect(user)

        questions_cf: List[int] = self._collaborative()
        questions_cbf: List[int] = self._content_based()

    def _collaborative(self) -> List[int]:
        suffix: str = "_10000"

        # Post.objects.filter(owner__=)

        return []

    def _content_based(self) -> List[int]:
        suffix: str = "_10000"

        total_sims: Dict[str, List[Tuple[int, float]]] = {}
        item_model = ItemModel.load(self.model)

        def create_similarities(data: List[Post]) -> List[Tuple[int, float]]:
            logger.debug("processing %ss" % key)
            for post in data:
                logger.debug("next item: %s [%d]" % (key, post.id))
                sims: List[Tuple[int, float]] = item_model.similarities(post, top_n=100)
                logger.debug("add similar items of %s [%d] to total similarity collection" % (key, post.id))
                total_sims[key].extend(sims)
            logger.debug("sort to total similarity collection")
            result = sorted(total_sims[key], key=lambda sim: sim[1], reverse=True)
            return result

        for key in [QKey, AKey]:

            total_sims[key] = []

            logger.info("fetching %ss of user [%d]: done" % (key, self.user_id))

            logger.info("creating similarities for %ss: start" % key)
            total_sims[key] = cache.get("%s_sims%s" % (key, suffix), create_similarities, self.data[key])
            logger.info("creating similarities for %ss: done" % key)

        question_sims_90 = list(filter(lambda q: q[1] > 0.9, total_sims[QKey]))
        answer_sims_90 = list(filter(lambda a: a[1] > 0.9, total_sims[AKey]))

        question_ids = set(map(lambda q: q[0], question_sims_90))
        answer_ids = set(map(lambda a: a[0], answer_sims_90))
        # intersection = list(filter(lambda q: q in answer_ids, question_ids))
        union = question_ids.union(answer_ids)

        return list(Post.objects.filter(id__in=union))
