import os
from enum import Enum
from importlib import import_module
from itertools import chain
from typing import Any, Dict, Type

from django.core.management import BaseCommand
from django.db.models import Model
from inflection import camelize, pluralize, singularize, underscore

import config
from app.blackbox.models import Doc2VecModel, ItemModel, LanguageModel, UserModel
from app.blackbox.models._lda_model import LDAModel
from app.blackbox.models._tfidf_model import TfidfModel
from app.utils import Logger

logger = Logger.get()


class Method(Enum):
    DOC2VEC = 'doc2vec'
    TFIDF = 'tfidf'
    LDA = 'lda'


class Command(BaseCommand):
    help = "pretrain a model offline"

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.method = None
        self.cls = "Post"

    def add_arguments(self, parser):
        parser.add_argument('--method', type=str, choices=[m.value for m in Method], default=Method.DOC2VEC.value)

    def handle(self, *args, **options):
        logger.info('training data...')

        (
            self.method,
        ) = [options[k] for k in ('method',)]

        self._pretrain()

        logger.info('done')

    def _pretrain(self):
        LanguageModel.create()

        model_name: str = singularize(camelize(self.cls))
        model_name_p: str = pluralize(camelize(self.cls))

        logger.info("Creating 'models.%s's" % model_name)

        try:
            model_cls: Type[Model] = getattr(import_module("app.models"), model_name)
        except (ImportError, AttributeError):
            raise ImportError(self.cls)

        logger.debug("loading all %s" % model_name_p)

        getattr(self, self.method)(model_cls)

    def doc2vec(self, model_cls: Type[Model]):
        path = os.path.join(config.DATA_PATH, config.MODELS_PATH)

        item_matches: Dict[str, Dict[str, Any]] = {
            "questions": {"post_type_id": 1},
            "answers": {"post_type_id": 2},
            # "posts": {}
        }

        user_matches: Dict[str, Dict[str, Dict[str, Any]]] = {
            "questions": {
                "num_questions": {
                    "op": ">",
                    "val": 0
                }
            },
            "answers": {
                "num_questions": {
                    "op": ">",
                    "val": 0
                },
                "num_answers": {
                    "op": ">",
                    "val": 0
                }
            }
        }

        models = {
            Doc2VecModel: Doc2VecModel.Method,
            TfidfModel: TfidfModel.Method,
            LDAModel: LDAModel.Method
        }

        for model, m in models.items():
            for method in m:
                for key, match in item_matches.items():
                    file_name = "{key}_{model_key}_{method}".format(
                        key=key,
                        model_key=underscore(model.__name__.replace("Model", "")),
                        method=underscore(method.value)
                    )
                    try:
                        ItemModel.load(file_name, path)
                    except FileNotFoundError as e:
                        item_model = ItemModel(model_cls, "body", match, model, method)
                        item_model.train()
                        item_model.save(file_name, path)

                for key, match in user_matches.items():
                    file_name = "{key}_{model_key}_{method}".format(
                        key=key,
                        model_key=underscore(model.__name__.replace("Model", "")),
                        method=underscore(method.value)
                    )
                    try:
                        UserModel.load(file_name, path)
                    except FileNotFoundError as e:
                        user_model = UserModel(match, {}, model, method)
                        user_model.train()
                        user_model.save(file_name, path)
