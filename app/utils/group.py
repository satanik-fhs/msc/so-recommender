from typing import Iterable, Any, Callable, Tuple, Dict, List


def group(iterable: Iterable[Any], predicate: Callable[[Any], Tuple[Any, Any]]) -> Dict[Any, List[Any]]:
    groups: Dict[Any, List[Any]] = {}
    for element in iterable:
        (key, value) = predicate(element)
        groups.setdefault(key, []).append(value)
    return groups
