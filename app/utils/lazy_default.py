from typing import Dict, Any, Callable


def lazy_default(d: Dict, key: Any, default: Callable[[], Any]) -> Any:
    return d[key] if key in d else d.setdefault(key, default())
