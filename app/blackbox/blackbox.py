import abc
from typing import Optional, Type

from django.db.models import Model

from app.blackbox import Similarity
# noinspection PyUnresolvedReferences
from app.blackbox.similarities import *


class IBlackBox(abc.ABC):
    pass


class BlackBox(IBlackBox):
    @staticmethod
    def calculate_similarity(a: Model, b: Model):
        cls_list = Similarity.__subclasses__()

        def annotations(x):
            return getattr(x, 'calculate_similarity').__annotations__

        def filter_annotations(x):
            r = annotations(x)
            return 'a' in r and 'b' in r and r['a'] == type(a) and r['b'] == type(b)

        all_matching = filter(filter_annotations, cls_list)

        similarity: Optional[Type[Similarity]] = next(all_matching, None)
        if similarity:
            return similarity().calculate_similarity(a, b)

    # @staticmethod
    # def get_longest_path(n: int = inf):
    #     np.random.seed(5432)
    #     space = np.random.randint(500, size=100)
    #     random.seed(5432)
    #     indices = random.sample(range(500, n))


