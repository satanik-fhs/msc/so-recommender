from __future__ import annotations

import abc
import os
import pickle
from typing import List, Tuple, Type

from django.db.models import Model
from inflection import underscore

import config
from app.blackbox.models import TextModel
from app.utils import Timer


class BaseModel(abc.ABC):

    MODELS = {}

    def __init__(self, model_type: Type[TextModel], method: TextModel.Method):
        self._method: Type[TextModel] = model_type
        self._model: TextModel = model_type(method)

    @abc.abstractmethod
    def train(self):
        pass

    def save(self, filename: str, path: str = None):
        if not path:
            path = config.DATA_PATH
        else:
            os.makedirs(path, mode=0o755, exist_ok=True)
        name = underscore(self.__class__.__name__.replace("Model", ""))
        file_path = "%s/%s.%s.model" % (path, filename, name)
        timer = Timer("saving %s to [%s]" % (self.__class__.__name__, file_path))
        model: TextModel = self._model
        self._model = None
        pickle.dump(self, open(file_path, "wb"))
        self._model = model
        self._model.save("%s.%s" % (filename, name), path)
        timer.stop()

    @classmethod
    def load(cls, filename: str, path: str = None) -> BaseModel:
        if not path:
            path = config.DATA_PATH

        name = underscore(cls.__name__.replace("Model", ""))
        file_path = "%s/%s.%s.model" % (path, filename, name)
        if file_path in BaseModel.MODELS:
            return BaseModel.MODELS[file_path]
        timer = Timer("loading item model from [%s]" % file_path)
        if not os.path.exists(file_path):
            raise FileNotFoundError("%s model not found at [%s]" % (name, file_path))
        model: BaseModel = pickle.load(open(file_path, "rb"))
        model._model = model._method.load("%s.%s" % (filename, name), path)
        BaseModel.MODELS[file_path] = model
        timer.stop()
        return model

    @abc.abstractmethod
    def similarities(self, obj: Model, top_n: int) -> List[Tuple[int, float]]:
        pass

    @abc.abstractmethod
    def similarity(self, a: Model, b: Model) -> float:
        pass
