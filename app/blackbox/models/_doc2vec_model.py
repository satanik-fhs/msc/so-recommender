import os
from gensim.models.doc2vec import Doc2Vec as gensim_doc2vec
from typing import Dict, Union, List, Tuple

import config
from app.blackbox.models._text_model import TextModel
from app.blackbox.models._corpus import Corpus
from app.utils import Logger, Timer, cpu_count

logger = Logger.get()


class Doc2VecModel(TextModel):
    class Method(TextModel.Method):
        PV_DBOW = "PV_DBOW"
        # PV_DM = "PV_DM"
        # PV_DM_CONCAT = "PV_DM_CONCAT"

    SETTINGS = {
        "PV_DBOW": {
            "vector_size": 100,
            "epochs": 10,
            "min_count": 2,
            "dm": 0,
            "batch_words": 1000,
            "sample": 0,
            "negative": 5,
            "hs": 0
        },
        "PV_DM": {
            "vector_size": 100,
            "epochs": 10,
            "window": 10,
            "min_count": 2,
            "alpha": 0.05,
            "dm": 1,
            "batch_words": 1000,
            "sample": 0,
            "negative": 5,
            "hs": 0
        },
        "PV_DM_CONCAT": {
            "vector_size": 200,
            "epochs": 20,
            "window": 5,  # range(3, 8, 2),
            "min_count": 2,  # range(1, 6, 2),
            "alpha": 0.05,
            "dm": 1,
            "dm_concat": 1,
            "batch_words": 1000,
            "sample": 0,
            "negative": 5,
            "hs": 0
        }
    }

    CORES = cpu_count()

    def __init__(self, method: Method):
        super().__init__(method)
        logger.info("creating Doc2Vec model with method [%s]" % method.value)
        self._setting: Dict[str, Union[int, float]] = Doc2VecModel.SETTINGS[method.value]
        self._model = None

    def train(self, corpus: Corpus):
        timer = Timer("training Doc2Vec model on [%d] cores" % Doc2VecModel.CORES)
        if not self._model:
            self._model = gensim_doc2vec(workers=Doc2VecModel.CORES, **self._setting)
        self._model.build_vocab(corpus)
        self._model.train(
            corpus,
            total_examples=self._model.corpus_count,
            epochs=self._model.epochs
        )
        timer.stop()

    def save(self, filename: str, path: str = None):
        if not path:
            path = config.DATA_PATH
        else:
            os.makedirs(path, mode=0o755, exist_ok=True)
        file_path = "%s/%s.doc2vec.model" % (path, filename)
        timer = Timer("saving Doc2Vec model to [%s]" % file_path)
        self._model.save(file_path)
        timer.stop()

    @classmethod
    def load(cls, filename: str, path: str = None):
        if not path:
            path = config.DATA_PATH
        file_path = "%s/%s.doc2vec.model" % (path, filename)
        timer = Timer("loading Doc2Vec model from [%s]" % file_path)
        model = Doc2VecModel(Doc2VecModel.Method.PV_DBOW)
        model._model = gensim_doc2vec.load(file_path)
        timer.stop()
        return model

    def similarities(self, text: Union[str, List[str]], top_n: int) -> List[Tuple[int, float]]:
        if not isinstance(text, list):
            text = text.split()

        timer = Timer("generating similarities from Doc2Vec for the top n [%d] items" % top_n)
        inferred_vector = self._model.infer_vector(text)
        sims = self._model.docvecs.most_similar([inferred_vector], topn=top_n)
        timer.stop()
        return sims

    def similarity(self, a: Union[str, List[str]], b: Union[str, List[str]]) -> float:
        if not isinstance(a, list):
            a = a.split()
        if not isinstance(b, list):
            b = b.split()
        timer = Timer("generate similarity from Doc2Vec between the two items")
        similarity = self._model.docvecs.similarity_unseen_docs(self._model, a, b)
        timer.stop()
        return similarity
