import abc

from django.db.models import Model


class Similarity(abc.ABC):

    @abc.abstractmethod
    def calculate_similarity(self, a: Model, b: Model):
        pass
