from typing import Type, List

from django.db.models import Model


class BulkCreator:
    model: Type[Model] = None
    chunk_size: int = None
    instances: List[Model] = None

    def __init__(self, model: Type[Model], chunk_size: int = None):
        self.model = model
        self.chunk_size = chunk_size
        self.instances = []

    def append(self, instance):
        self.instances.append(instance)

        if self.chunk_size and len(self.instances) >= self.chunk_size:
            self.create()
            self.instances.clear()

    def create(self):
        if len(self.instances) > 0:
            self.model.objects.bulk_create(self.instances)
