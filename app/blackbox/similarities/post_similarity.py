from spacy.tokens import Doc

from app.blackbox import Similarity
from app.models import Post
from app.utils import Logger
from app.blackbox.models import LanguageModel

logger = Logger.get()


class PostSimilarity(Similarity):

    def calculate_similarity(self, a: Post, b: Post):
        doc_a: Doc = LanguageModel.preprocess_text(a.body)
        doc_b: Doc = LanguageModel.preprocess_text(b.body)
        return doc_a.similarity(doc_b)
