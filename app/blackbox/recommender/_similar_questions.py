from typing import Iterable, List, Tuple

from app.blackbox.recommender import ItemModelRecommender
from app.models import Post, User


class SimilarQuestions(ItemModelRecommender):

    def recommend(self, a: User, top_n: int = 10) -> List[int]:
        item_model = self._get_item_model()
        questions: List[Post] = a.posts.filter(post_type_id=Post.Type.QUESTION).all()
        question_ids: Iterable[int] = map(lambda q: q.id, questions)
        recommendations: List[Tuple[int, float]] = []
        for question in questions:
            recommendations.extend(item_model.similarities(question, top_n=top_n))
        ids: Iterable[int] = map(lambda t: t[0], filter(lambda t: t[0] not in question_ids,  recommendations))
        return list(ids)
