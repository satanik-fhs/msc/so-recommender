from typing import Iterable, List, Tuple

from app.blackbox.models import UserModel
from app.blackbox.recommender import UserModelRecommender
from app.models import Post, User
from app.utils import Timer


class QuestionsAnsweredBySimilarUsers(UserModelRecommender):

    def recommend(self, a: User, top_n: int = 10) -> List[int]:
        user_model: UserModel = self._get_user_model()
        timer = Timer("sim")
        users: List[Tuple[int, float]] = user_model.similarities(a, top_n=top_n)
        timer.stop()
        ids: Iterable[int] = map(lambda t: t[0], users)
        posts: List[Post] = Post.objects.raw("""
        SELECT q.*
        FROM recs_post q INNER JOIN recs_post a ON q.id = a.parent_id
        WHERE
            (q.post_type_id = 1) AND
            (a.post_type_id = 2) AND
            (a.owner_user_id IN (%s))
        """, [",".join(map(lambda id: str(id), ids))])
        return list(map(lambda p: p.id, posts))
